# Using Data Version Control with git

Testing using data version control for data with git

## What is DVC?

Data version control (DVC) is the cool concept of tracking data files
in a similar way that we track versions using git. Read more at [https://dvc.org/](https://dvc.org/).

## This test

There is an Excel file in the `data/` folder that is pushed/pulled
from a shared Google drive. There is a test Python script in `code/`
that prints a single cell from the Excel file. When configuring data
with DVC, it adds a gitignore file to ignore the data file itself, but
it adds text files with info that are then git-tracked.

To run this test you will need:  
 * to have DVC installed on your machine
 * to have an Anaconda environment with `pandas` installed
 * A google account you will use for accessing the shared data
 * to ask Jenni for access to the shared Google drive used for testing
  (be sure to include your google email)

To get the Excel file (assuming you've been granted access to the Google Drive
folder), follow the section below about pulling data.

## DVC workflow

Here are some example workflows, but they might be broken. The documentation
at [https://dvc.org/doc](https://dvc.org/doc) is a great reference.

### Pulling data from remote (update local)

If your colleague has pushed data and you want to pull it.

The git command that the data has been committed to master branch.
```
git pull origin master
dvc pull
```

Now you should have an updated version of the data in your repository.

### Push data to remote (update server)

You've regenerated and need to push the results so your colleagues can
pull and analyse them.

The git command assumes you are committing directly on master branch
```
dvc add data/data.xlsx
git commit data/data.xlsx.dvc -m "Dataset updates"
dvc push -r <remote_name>
git push origin master
```

