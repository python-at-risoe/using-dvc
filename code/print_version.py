"""Load the data and print the statement"""


if __name__ == '__main__':
    import pandas as pd
    data = '../data/data.xlsx'
    df = pd.read_excel(data, header=None)
    print(df.values[0, 0])
